# TP NIX

## Collaboration

Fait par Pauthier Antoine et Arthur Gianini

## Nix

installation de nix

```bash
sh <(curl -L https://nixos.org/nix/install) --daemon
```

ajout du dossier et config

```bash
mkdir -p ~/.config/nix/
echo "experimental-features = nix-command flakes" > ~/.config/nix/nix.conf
```

installation de direnv et git

```bash
nix profile install nixpkgs#direnv
nix profile install nixpkgs#git
```

ajout des export dans .bashrc

```bash
nano ~/.bashrc

export DIRENV_WARN_TIMEOUT=40s
export DIRENV_LOG_FORMAT=""
eval "$(direnv hook ${SHELL/*\//})"
...
```

git clone du repo

```bash
git clone https://gitlab.com/alsim/opensource.git
Cloning into 'opensource'...
remote: Enumerating objects: 593, done.
...
```

on clone le repo et on l’accède (l’erreur s’affiche)

```bash
cd opensource-arthur-gianini/system/tp
direnv: error /home/arthur/Documents/tp/opensource-arthur-gianini/system/tp/.envrc is blocked. Run `direnv allow` to approve its content
```

un fichier nix qui sert à organiser les dépendances (ici les flakes) ainsi que les fonctions ayant besoins des dépendances. Cette organisation est faite sous forme d'input/output, ou l'input sont les dépendances flake et l'output sont les retours de fonctions qui utilisent les flakes.

### fichier

```bash
{
#Une description courte du flake
  description = "Open source courses";

#Les dependances du flake
  inputs = {
    nixpkgs = {
      url = "github:NixOS/nixpkgs";
    };

    flake-utils = {
      url = "github:numtide/flake-utils";
    };
  };
#Une ou des fonctions qui prennent en argument les flake et qui ressortent les resultats
  outputs = inputs @ { self, ... }:
    inputs.flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import inputs.nixpkgs {
          inherit system;
        };
      in
      {
        devShells.default =
          pkgs.mkShell
            {

              nativeBuildInputs = with pkgs;
                [
                  lefthook
                  go-task
                  marp-cli
                  typos
                  drawio
                  nodejs_18
                ];

              devShellHook = ''
                task init
              '';
            };
      });
}
```

### Qu'est ce que nixpkgs ?

nixpkgs est un flake (dépendance) recupéré par URL, ici github

### Pourquoi c'est bizarre ?

- L'arborescence du fichier (input output) est un peu atypique, ainsi qu'utiliser directement les dépendances comme argument de fonction

creation du directory et le contenu de **containers.conf** et **policy.json**

```bash
mkdir -p ~/.config/containers/

cat <<EOF > ~/.config/containers/containers.conf
[engine]
events_logger = "file"
cgroup_manager = "cgroupfs"
EOF
cat <<EOF > ~/.config/containers/policy.json 
{
  "default": [
    { "type": "insecureAcceptAnything" }
  ],
  "transports": {
    "docker-daemon": {
      "": [
        { "type": "insecureAcceptAnything" }
      ]
    }
  }
}
EOF
```

ajout de uidmap

```bash
sudo apt install -y uidmap
[sudo] password for arthur: 
Reading package lists... Done
...
```

Création du fichier Containerfile

```bash
nano Containerfile

FROM docker.io/alpine:latest

ENV \
    PATH="/root/.cargo/bin:$PATH"

RUN \
  apk add \
    cargo \
  && cargo install tealdeer \
  && tldr --update 

ENTRYPOINT [ "bash", "-c", "tldr -update && tldr $@"]
CMD []
```

création du container

```bash
podman build -t tldr .

podman build -t tldr .
STEP 1/5: FROM docker.io/alpine:latest
Trying to pull docker.io/library/alpine:latest...
Getting image source signatures
...
```

mot clé CMD

CMD établit l'exécutable par défaut dans l’image.

Mot clé ENTRYPOINT

ENTRYPOINT spécifie les exécutables qui seront exécutés après le démarrage du conteneur

on teste hadolint

```bash
hadolint Containerfile
Containerfile:1 DL3007 warning: Using latest is prone to errors if the image will ever update. Pin the version explicitly to a release tag
Containerfile:6 DL3018 warning: Pin versions in apk add. Instead of `apk add <package>` use `apk add <package>=<version>`
Containerfile:6 DL3019 info: Use the `--no-cache` switch to avoid the need to use `--update` and remove `/var/cache/apk/*` when done installing packages
```

on répare les warnings en mettant des versions fixes

```bash
FROM docker.io/alpine:3.18.4

ENV \
    PATH="/root/.cargo/bin:$PATH"

RUN \
  apk add \
    cargo=1.72.1-r0 \
  && cargo install tealdeer \
  && tldr --update 

ENTRYPOINT [ "bash", "-c", "tldr -update && tldr $@"]
CMD []
```
